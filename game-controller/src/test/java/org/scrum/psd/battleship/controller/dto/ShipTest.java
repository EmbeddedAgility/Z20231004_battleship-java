package org.scrum.psd.battleship.controller.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ShipTest {
    @Test
    void whenShipIsPlacedGivenShipPlacedDiagonallyThenReturnFalse() {
        Ship ship = new Ship();
        ship.addPosition("A1");
        assertFalse(ship.addPosition("B2"));
    }

    @Test
    void whenShipIsPlacedGivenShipPlacedWithSpaceThenReturnFalse() {
        Ship ship = new Ship();
        ship.addPosition("A1");
        assertFalse(ship.addPosition("A3"));
    }

    @Test
    void whenShipIsPlacedGivenShipPlacedCorrectlyThenReturnTrue() {
        Ship ship = new Ship();
        ship.addPosition("A1");
        assertTrue(ship.addPosition("A2"));
    }
}