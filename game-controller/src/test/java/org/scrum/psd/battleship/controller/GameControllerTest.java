package org.scrum.psd.battleship.controller;

import org.junit.Assert;
import org.junit.Test;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameControllerTest {
    @Test
    public void testCheckIsHitTrue() {
        List<Ship> ships = GameController.initializeShips();
        int counter = 0;

        for (Ship ship : ships) {
            Letter letter = Letter.values()[counter];

            for (int i = 0; i < ship.getSize(); i++) {
                ship.getPositions().add(new Position(letter, i));
            }

            counter++;
        }

        boolean result = GameController.checkIsHit(ships, new Position(Letter.A, 1));

        Assert.assertTrue(result);
    }

    @Test
    public void testCheckIsHitFalse() {
        List<Ship> ships = GameController.initializeShips();
        int counter = 0;

        for (Ship ship : ships) {
            Letter letter = Letter.values()[counter];

            for (int i = 0; i < ship.getSize(); i++) {
                ship.getPositions().add(new Position(letter, i));
            }

            counter++;
        }

        boolean result = GameController.checkIsHit(ships, new Position(Letter.H, 1));

        Assert.assertFalse(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckIsHitPositstionIsNull() {
        GameController.checkIsHit(GameController.initializeShips(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckIsHitShipIsNull() {
        GameController.checkIsHit(null, new Position(Letter.H, 1));
    }

    @Test
    public void testIsShipValidFalse() {
        Ship ship = new Ship("TestShip", 3);
        boolean result = GameController.isShipValid(ship);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsShipValidTrue() {
        List<Position> positions = Arrays.asList(new Position(Letter.A, 1), new Position(Letter.A, 1), new Position(Letter.A, 1));
        Ship ship = new Ship("TestShip", 3, positions);

        boolean result = GameController.isShipValid(ship);

        Assert.assertTrue(result);
    }

    @Test
    public void testIsWinTrueWhenAllShipsSunk() {
        List<Ship> ships = new ArrayList<>();
        List<Position> positions = Arrays.asList(new Position(Letter.A, 1), new Position(Letter.A, 2), new Position(Letter.A, 3));
        Ship ship = new Ship("TestShip", 3, positions);
        ships.add(ship);
        List<Position> shots = Arrays.asList(new Position(Letter.A, 1), new Position(Letter.A, 2), new Position(Letter.A, 3));
        boolean result = GameController.checkIsWin(ships, shots);
        Assert.assertTrue(result);
    }

    @Test
    public void testIsWinFalseWhenNotAllShipsSunk() {
        List<Ship> ships = new ArrayList<>();
        List<Position> positions = Arrays.asList(new Position(Letter.A, 1), new Position(Letter.A, 2), new Position(Letter.A, 3));
        Ship ship = new Ship("TestShip", 3, positions);
        ships.add(ship);
        List<Position> shots = Arrays.asList(new Position(Letter.A, 1), new Position(Letter.A, 3), new Position(Letter.A, 3));
        boolean result = GameController.checkIsWin(ships, shots);
        Assert.assertFalse(result);
    }
}
