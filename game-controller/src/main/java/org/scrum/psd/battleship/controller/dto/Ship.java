package org.scrum.psd.battleship.controller.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Ship {
    private boolean isPlaced;
    private String name;
    private int size;
    private List<Position> positions;
    private Color color;

    public Ship() {
        this.positions = new ArrayList<>();
    }

    public Ship(String name, int size) {
        this();

        this.name = name;
        this.size = size;
    }

    public Ship(String name, int size, List<Position> positions) {
        this(name, size);

        this.positions = positions;
    }

    public Ship(String name, int size, Color color) {
        this(name, size);

        this.color = color;
    }

    public void isValidPositionToAdd(String input) throws Exception {
        if (positions == null) {
            positions = new ArrayList<>();
        }

        Letter letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
        int number = Integer.parseInt(input.substring(1));
        Position proposedPosition = new Position(letter, number);
        if (!isShipPositionedCorrectly(positions, proposedPosition)) {
            throw new IllegalArgumentException("Failed to add position. The ship can only be placed horizontally or vertically.");
        }
    }

    public boolean addPosition(String input) {
        if (positions == null) {
            positions = new ArrayList<>();
        }

        Letter letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
        int number = Integer.parseInt(input.substring(1));
        Position proposedPosition = new Position(letter, number);
        if (isShipPositionedCorrectly(positions, proposedPosition)) {
            positions.add(proposedPosition);
            return true;
        } else {
            return false;
        }
    }

    protected boolean isShipPositionedCorrectly(List<Position> positions, Position proposedPosition) {
        switch (positions.size()) {
            case 0:
                return true;
            case 1:
                int currentRow = positions.get(0).getRow();
                Letter currentCol = positions.get(0).getColumn();
                ArrayList<Position> validNextPositions = new ArrayList<>();
                if (currentCol != Letter.A) {
                    validNextPositions.add(new Position(currentCol.previous(), currentRow));
                }
                if (currentCol != Letter.H) {
                    validNextPositions.add(new Position(currentCol.next(), currentRow));
                }
                if (currentRow != 1) {
                    validNextPositions.add(new Position(currentCol, currentRow - 1));
                }
                if (currentRow != 8) {
                    validNextPositions.add(new Position(currentCol, currentRow + 1));
                }
                if (validNextPositions.contains(proposedPosition)) {
                    return true;
                }
                break;
            default: {
                List<Letter> columns = positions.stream().map(Position::getColumn).collect(Collectors.toList());
                List<Integer> rows = positions.stream().map(Position::getRow).collect(Collectors.toList());
                boolean isVerticalShip = columns.stream().distinct().count() == 1;
                if (isVerticalShip) {
                    return proposedPosition.getColumn() == columns.get(0) && getValidRows(rows).contains(proposedPosition.getRow());
                }
                boolean isHorizontalShip = rows.stream().distinct().count() == 1;
                if (isHorizontalShip) {
                    return proposedPosition.getRow() == rows.get(0) && getValidColumns(columns).contains(proposedPosition.getColumn());
                }
                return false;
            }
        }
        return false;
    }

    public static List<Integer> getValidRows(List<Integer> rows) {
        List<Integer> validRows = new ArrayList<Integer>();
        Integer maxRow = Collections.max(rows);
        Integer minRow = Collections.min(rows);
        if (maxRow != 8) {
            validRows.add(maxRow + 1);
        }
        if (minRow != 1) {
            validRows.add(minRow - 1);
        }
        return validRows;
    }

    public static List<Letter> getValidColumns(List<Letter> columns) {
        List<Letter> validColumns = new ArrayList<Letter>();
        Letter maxColumn = Collections.max(columns);
        Letter minColumn = Collections.min(columns);
        if (maxColumn != Letter.H) {
            validColumns.add(maxColumn.next());
        }
        if (minColumn != Letter.A) {
            validColumns.add(minColumn.previous());
        }
        return validColumns;
    }


    // TODO: property change listener implementieren

    public boolean isPlaced() {
        return isPlaced;
    }

    public void setPlaced(boolean placed) {
        isPlaced = placed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
