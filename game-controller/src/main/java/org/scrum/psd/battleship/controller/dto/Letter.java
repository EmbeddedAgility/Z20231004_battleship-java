package org.scrum.psd.battleship.controller.dto;

public enum Letter {
    A, B, C, D, E, F, G, H;

    public Letter previous() {
        return values()[(ordinal() - 1  + values().length) % values().length];
    }

    public Letter next() {
        return values()[(ordinal() + 1) % values().length];
    }
}
