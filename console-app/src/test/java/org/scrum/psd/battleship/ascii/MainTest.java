package org.scrum.psd.battleship.ascii;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Execution(ExecutionMode.CONCURRENT)
public class MainTest {
    @Test
    void ifValidPositionInputDoNotThrowError() throws Exception {
        Assertions.assertDoesNotThrow(() -> Main.isInputValid("A1"));
    }

    @Test
    void ifInvalidLetterPositionInputThrowError() throws Exception {
        Assertions.assertThrows(Exception.class, () -> Main.isInputValid("L1"));
    }

    @Test
    void ifInvalidNumberPositionInputThrowError() throws Exception {
        Assertions.assertThrows(Exception.class, () -> Main.isInputValid("A0"));
    }

    @Test
    void ifSmallAlphabetPositionInputShouldNotThrowError() throws Exception {
        Assertions.assertDoesNotThrow(() -> Main.isInputValid("a5"));
    }

    @Test
    void ifOccupiedPositionInputThrowError() throws Exception {
        List<Position> occupiedPositions = List.of(new Position(Letter.A, 1));
        Assertions.assertThrows(Exception.class, () -> Main.validateOverlap(occupiedPositions, new Position(Letter.A, 1)));
    }

    @Test
    void ifUnoccupiedPositionInputShouldNotThrowError() throws Exception {
        List<Position> occupiedPositions = List.of(new Position(Letter.A, 1));
        Assertions.assertDoesNotThrow(() -> Main.validateOverlap(occupiedPositions, new Position(Letter.A, 2)));
    }

    @Test
    void testPrintGoodMessage() {
        String result = Main.getMessage(true, "good", "");

        // Assert that the output contains the expected ANSI escape codes for red and green
        Assertions.assertEquals(result, Main.GREEN + "good" + Main.RESET);
    }

    @Test
    void testPrintBadMessage() {
        String result = Main.getMessage(false, "", "bad");

        // Assert that the output contains the expected ANSI escape codes for red and green
        Assertions.assertEquals(result, Main.RED + "bad" + Main.RESET);
    }

    @Test
    void testGetSunkedEnemyFleet() {
        List<Ship> enemyFleet = new ArrayList<>();
        List<Position> positions = Arrays.asList(new Position(Letter.A, 1), new Position(Letter.A, 2), new Position(Letter.A, 3));
        Ship ship = new Ship("TestShip", 3, positions);
        enemyFleet.add(ship);
        List<Position> shots = Arrays.asList(new Position(Letter.A, 1), new Position(Letter.A, 2), new Position(Letter.A, 3));
        String result = Main.getSunkedEnemyFleet(enemyFleet, shots);
        Assertions.assertEquals("TestShip", result);

    }@Test
    void testGetRemainingEnemyFleet() {
        List<Ship> enemyFleet = new ArrayList<>();
        List<Position> positions = Arrays.asList(new Position(Letter.A, 1), new Position(Letter.A, 2), new Position(Letter.A, 3));
        Ship ship = new Ship("TestShip", 3, positions);
        enemyFleet.add(ship);
        List<Position> shots = Arrays.asList(new Position(Letter.A, 1), new Position(Letter.A, 3), new Position(Letter.A, 3));
        String result = Main.getRemainingEnemyFleet(enemyFleet, shots);
        Assertions.assertEquals("TestShip", result);

    }
}
