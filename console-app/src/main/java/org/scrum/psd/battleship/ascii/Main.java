package org.scrum.psd.battleship.ascii;

import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.*;
import java.util.stream.Collectors;

import static com.diogonunes.jcolor.Ansi.colorize;
import static com.diogonunes.jcolor.Attribute.MAGENTA_TEXT;

public class Main {
    private static List<Ship> myFleet;
    private static List<Ship> enemyFleet;

    private static List<Position> myShots = new ArrayList<>();

    private static List<Position> enemyShots = new ArrayList<>();
    private static final Telemetry telemetry = new Telemetry();
    static String RED = "\u001B[31m";
    static String GREEN = "\u001B[32m";
    static String RESET = "\u001B[0m";

    public static void main(String[] args) {
        telemetry.trackEvent("ApplicationStarted", "Technology", "Java");
        System.out.println(colorize("                                     |__", MAGENTA_TEXT()));
        System.out.println(colorize("                                     |\\/", MAGENTA_TEXT()));
        System.out.println(colorize("                                     ---", MAGENTA_TEXT()));
        System.out.println(colorize("                                     / | [", MAGENTA_TEXT()));
        System.out.println(colorize("                              !      | |||", MAGENTA_TEXT()));
        System.out.println(colorize("                            _/|     _/|-++'", MAGENTA_TEXT()));
        System.out.println(colorize("                        +  +--|    |--|--|_ |-", MAGENTA_TEXT()));
        System.out.println(colorize("                     { /|__|  |/\\__|  |--- |||__/", MAGENTA_TEXT()));
        System.out.println(colorize("                    +---------------___[}-_===_.'____                 /\\", MAGENTA_TEXT()));
        System.out.println(colorize("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _", MAGENTA_TEXT()));
        System.out.println(colorize(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7", MAGENTA_TEXT()));
        System.out.println(colorize("|                        Welcome to Battleship                         BB-61/", MAGENTA_TEXT()));
        System.out.println(colorize(" \\_________________________________________________________________________|", MAGENTA_TEXT()));
        System.out.println("");

        InitializeGame();

        StartGame();
    }

    private static void StartGame() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("\033[2J\033[;H");
        System.out.println("                  __");
        System.out.println("                 /  \\");
        System.out.println("           .-.  |    |");
        System.out.println("   *    _.-'  \\  \\__/");
        System.out.println("    \\.-'       \\");
        System.out.println("   /          _/");
        System.out.println("  |      _  /\" \"");
        System.out.println("  |     /_\'");
        System.out.println("   \\    \\_/");
        System.out.println("    \" \"\" \"\" \"\" \"");

        boolean isMyWin;
        boolean isEnemyWin;
        int round = 1;
        do {
            System.out.println("##############################################################");
            System.out.println("Turn: " + round++);
            System.out.println("##############################################################");
            printEnemyFleet();
            System.out.println("Player, it's your turn");
            System.out.println("Enter coordinates for your shot :");
            Position position = parsePosition(scanner);
            boolean isHit = GameController.checkIsHit(enemyFleet, position);
            myShots.add(position);
            if (isHit) {
                beep();

                System.out.println("                \\         .  ./");
                System.out.println("              \\      .:\" \";'.:..\" \"   /");
                System.out.println("                  (M^^.^~~:.'\" \").");
                System.out.println("            -   (/  .    . . \\ \\)  -");
                System.out.println("               ((| :. ~ ^  :. .|))");
                System.out.println("            -   (\\- |  \\ /  |  /)  -");
                System.out.println("                 -\\  \\     /  /-");
                System.out.println("                   \\  \\   /  /");
            }
            printMessage(isHit, "Yeah ! Nice hit !", "Miss");
            telemetry.trackEvent("Player_ShootPosition", "Position", position.toString(), "IsHit", Boolean.valueOf(isHit).toString());

            position = getRandomPosition();
            isHit = GameController.checkIsHit(myFleet, position);
            enemyShots.add(position);
            System.out.println("");

            System.out.print(String.format("Computer shoot in %s%s and ", position.getColumn(), position.getRow()));
            printMessage(isHit, "miss", "hit your ship !");
            telemetry.trackEvent("Computer_ShootPosition", "Position", position.toString(), "IsHit", Boolean.valueOf(isHit).toString());
            if (isHit) {
                beep();

                System.out.println("                \\         .  ./");
                System.out.println("              \\      .:\" \";'.:..\" \"   /");
                System.out.println("                  (M^^.^~~:.'\" \").");
                System.out.println("            -   (/  .    . . \\ \\)  -");
                System.out.println("               ((| :. ~ ^  :. .|))");
                System.out.println("            -   (\\- |  \\ /  |  /)  -");
                System.out.println("                 -\\  \\     /  /-");
                System.out.println("                   \\  \\   /  /");

            }
            System.out.println("**************************************************************");
            System.out.println("");
            isMyWin = GameController.checkIsWin(enemyFleet, myShots);
            isEnemyWin = GameController.checkIsWin(myFleet, enemyShots);
        } while (!isMyWin && (!isEnemyWin));
        if (isMyWin)
            printMessage(true, "You are the winner!", "");
        else
            printMessage(false, "", "You lost!");
    }

    public static void printMessage(boolean isGood, String goodMessage, String badMessage) {
        System.out.println(getMessage(isGood, goodMessage, badMessage));
    }

    public static String getMessage(boolean isGood, String goodMessage, String badMessage) {
        return isGood ? GREEN + goodMessage + RESET : RED + badMessage + RESET;
    }

    private static void beep() {
        System.out.print("\007");
    }

    protected static Position parsePosition(Scanner scanner) {
        boolean isPositionValid = false;
        Position proposedPosition = null;
        while (!isPositionValid) {
            try {
                proposedPosition = isInputValid(scanner.next());
                isPositionValid = true;
            } catch (Exception e) {
                printMessage(false, "", e.getMessage());
                System.out.println("Please re-enter position:");
            }
        }
        return proposedPosition;
    }

    private static Position getRandomPosition() {
        int rows = 8;
        int lines = 8;
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(lines)];
        int number = random.nextInt(rows);
        Position position = new Position(letter, number);
        return position;
    }

    private static void InitializeGame() {
        InitializeMyFleet();

        InitializeEnemyFleet();
    }

    private static void InitializeMyFleet() {
        Scanner scanner = new Scanner(System.in);
        myFleet = GameController.initializeShips();

        System.out.println("Please position your fleet (Game board has size from A to H and 1 to 8) :");

        for (Ship ship : myFleet) {
            System.out.println("");
            System.out.println(String.format("Please enter the positions for the %s (size: %s)", ship.getName(), ship.getSize()));
            for (int i = 1; i <= ship.getSize(); i++) {
                System.out.println(String.format("Enter position %s of %s (i.e A3):", i, ship.getSize()));
                List<Position> allOccupiedPositions = myFleet.stream().map(Ship::getPositions).flatMap(Collection::stream).collect(Collectors.toList());
                String positionInput = getValidPositionInput(scanner, ship, allOccupiedPositions);
                ship.addPosition(positionInput);

                telemetry.trackEvent("Player_PlaceShipPosition", "Position", positionInput, "Ship", ship.getName(), "PositionInShip", Integer.valueOf(i).toString());
            }
        }
    }

    protected static String getValidPositionInput(Scanner scanner, Ship ship, List<Position> allOccupiedPositions) {
        boolean isPositionValid = false;
        String positionInput = "";
        while (!isPositionValid) {
            positionInput = scanner.next();
            try {
                Position proposedPosition = isInputValid(positionInput);
                validateOverlap(allOccupiedPositions, proposedPosition);
                ship.isValidPositionToAdd(positionInput);
                isPositionValid = true;
            } catch (Exception e) {
                printMessage(false, "", e.getMessage());
                System.out.println("Please re-enter position:");
            }
        }
        return positionInput;
    }

    protected static void validateOverlap(List<Position> allOccupiedPositions, Position proposedPosition) {
        if (allOccupiedPositions.contains(proposedPosition)) {
            throw new IllegalArgumentException("Position is already occupied.");
        }
    }

    protected static Position isInputValid(String positionInput) throws Exception {
        try {
            Letter letter = Letter.valueOf(positionInput.toUpperCase().substring(0, 1));
            int number = Integer.parseInt(positionInput.substring(1));
            if (number < 1 || number > 8) {
                throw new NumberFormatException("number should be between 1 and 8");
            }
            return new Position(letter, number);
        } catch (Exception e) {
            throw new IllegalArgumentException("Please give a valid input that exists on the board. Game board has size from A to H and 1 to 8.");
        }
    }

    private static void InitializeEnemyFleet() {
        enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 5));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 7));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 8));

        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 5));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 7));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 8));

        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 3));

        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 8));

        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 5));
        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 6));
    }

    public static String getSunkedEnemyFleet(List<Ship> enemyFleet, List<Position> myShots) {
        String result = "";
        for (Ship ship : enemyFleet.stream().filter(s -> !(s == null) && myShots.containsAll(s.getPositions())).collect(Collectors.toList())) {
            result += ship.getName() + ", ";
        }
        return result.length() > 2 ? result.substring(0, result.length() - 2) : result;
    }

    public static String getRemainingEnemyFleet(List<Ship> enemyFleet, List<Position> myShots) {
        String result = "";
        for (Ship ship : enemyFleet.stream().filter(s -> !(s == null) && !myShots.containsAll(s.getPositions())).collect(Collectors.toList())) {
            result += ship.getName() + ", ";
        }
        return result.length() > 2 ? result.substring(0, result.length() - 2) : result;
    }

    private static void printEnemyFleet() {
        if (getSunkedEnemyFleet(enemyFleet, myShots).length() > 0)
            System.out.println(GREEN + "Ships sunked: " + getSunkedEnemyFleet(enemyFleet, myShots) + RESET);
        if (getRemainingEnemyFleet(enemyFleet, myShots).length() > 0)
            System.out.println(RED + "Ships remaining: " + getRemainingEnemyFleet(enemyFleet, myShots) + RESET);

    }
}
